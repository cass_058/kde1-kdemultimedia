msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 1999-04-14 03:36+0200\n"
"PO-Revision-Date: 1998-09-07 22:37+SET\n"
"Last-Translator: Christer Gustavsson <f93-cgn@sm.luth.se>\n"
"Language-Team: SWEDISH <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING\n"

# kmidframe.cpp:182
#: kmidframe.cpp:64
#, fuzzy
msgid "Play/Pause"
msgstr "Paus"

# kmidframe.cpp:173
#: kmidframe.cpp:68 kmidframe.cpp:209
msgid "Previous Song"
msgstr "F�reg�ende s�ng"

# kmidframe.cpp:192
#: kmidframe.cpp:70 kmidframe.cpp:228
msgid "Next Song"
msgstr "N�sta s�ng"

# kmidframe.cpp:113
#: kmidframe.cpp:72 kmidframe.cpp:142
msgid "See &Text events"
msgstr "Se &texth�ndelser"

# kmidframe.cpp:117
#: kmidframe.cpp:74 kmidframe.cpp:148
msgid "See &Lyrics events"
msgstr "Se s&�ngtexth�ndelser"

#: kmidframe.cpp:76
msgid "Scroll down karaoke"
msgstr ""

#: kmidframe.cpp:78
msgid "Scroll up karaoke"
msgstr ""

#: kmidframe.cpp:80
msgid "Scroll page down karaoke"
msgstr ""

#: kmidframe.cpp:82
msgid "Scroll page up karaoke"
msgstr ""

# kmidframe.cpp:65
#: kmidframe.cpp:91
msgid "&Save Lyrics ..."
msgstr "&Spara s�ngtexter ..."

# kmidframe.cpp:71
#: kmidframe.cpp:97
msgid "&Play"
msgstr "S&pela"

# kmidframe.cpp:73
#: kmidframe.cpp:99
msgid "P&ause"
msgstr "Pa&us"

# kmidframe.cpp:74
#: kmidframe.cpp:100
msgid "&Stop"
msgstr "S&top"

# kmidframe.cpp:77
#: kmidframe.cpp:104
msgid "P&revious Song"
msgstr "&F�reg�ende s�ng"

# kmidframe.cpp:79
#: kmidframe.cpp:107
msgid "&Next Song"
msgstr "&N�sta s�ng"

# kmidframe.cpp:82
#: kmidframe.cpp:111
msgid "&Loop"
msgstr "Cirku&lera"

# kmidframe.cpp:88
#: kmidframe.cpp:117
msgid "&Organize ..."
msgstr "&Organisera ..."

# kmidframe.cpp:91
#: kmidframe.cpp:120
msgid "In order mode"
msgstr "Ordnat l�ge"

# kmidframe.cpp:94
#: kmidframe.cpp:123
msgid "Shuffle mode"
msgstr "Blandat l�ge"

# kmidframe.cpp:98
#: kmidframe.cpp:127
msgid "AutoAdd to Collection"
msgstr "L�gg till automatiskt i samling"

# kmidframe.cpp:104
#: kmidframe.cpp:133
msgid "&General Midi file"
msgstr "&Allm�n midifil"

# kmidframe.cpp:108
#: kmidframe.cpp:137
msgid "&MT-32 file"
msgstr "&MT-32 fil"

# kmidframe.cpp:122
#: kmidframe.cpp:154
msgid "&Automatic Text chooser"
msgstr "&Automatisk textv�ljare"

# kmidframe.cpp:128
#: kmidframe.cpp:160
msgid "Show &Volume Bar"
msgstr "Visa &volymrad"

# kmidframe.cpp:133
#: kmidframe.cpp:165
msgid "Show &Channel View"
msgstr "Visa &kanal vy"

# kmidframe.cpp:138
#: kmidframe.cpp:170
msgid "Channel View &Options"
msgstr "Kanal vy&inst�llningar"

# kmidframe.cpp:142
#: kmidframe.cpp:174
msgid "&Font Change ..."
msgstr "&�ndra typsnitt"

#: kmidframe.cpp:176
msgid "Configure &Keys ..."
msgstr ""

# kmidframe.cpp:145
#: kmidframe.cpp:179
msgid "Midi &Setup ..."
msgstr "Inst�llningar f�r &midi"

# kmidframe.cpp:150
#: kmidframe.cpp:184
#, fuzzy, c-format
msgid ""
"%s\n"
"\n"
"(C) 1997,98 Antonio Larrosa Jimenez\n"
"larrosa@kde.org\t\tantlarr@arrakis.es\n"
"Malaga (Spain)\n"
"\n"
"Midi/Karaoke file player\n"
"KMid's homepage is at : http://www.arrakis.es/~rlarrosa/kmid.html\n"
"\n"
"KMid comes with ABSOLUTELY NO WARRANTY; for details view file COPYING\n"
"This is free software, and you are welcome to redistribute it\n"
"under certain conditions\n"
msgstr ""
"%s\n"
"\n"
"(C) 1997,98 Antonio Larrosa Jimenez (antlarr@arrakis.es)\n"
"Malaga (Spain)\n"
"\n"
"Midi/Karaoke file player\n"
"\n"
"KMid comes with ABSOLUTELY NO WARRANTY; for details view file COPYING\n"
"This is free software, and you are welcome to redistribute it\n"
"under certain conditions\n"

# kmidframe.cpp:161
#: kmidframe.cpp:197
msgid "&Song"
msgstr "&S�ng"

# kmidframe.cpp:162
#: kmidframe.cpp:198
msgid "&Collections"
msgstr "&Samlingar"

# kmidframe.cpp:176
#: kmidframe.cpp:212
msgid "Rewind"
msgstr "Spola tillbaka"

# kmidframe.cpp:182
#: kmidframe.cpp:218
msgid "Pause"
msgstr "Paus"

# kmidframe.cpp:186
#: kmidframe.cpp:222
msgid "Play"
msgstr "Spela"

# kmidframe.cpp:189
#: kmidframe.cpp:225
msgid "Forward"
msgstr "Fram�t"

# kmidframe.cpp:198
#: kmidframe.cpp:234
msgid "Show Channel View"
msgstr "Visa kanalvy"

# kmidframe.cpp:203
#: kmidframe.cpp:239
msgid "Show Volume Bar"
msgstr "Visa volymrad"

# kmidframe.cpp:520
#: kmidframe.cpp:562
msgid ""
"Couldn't open /dev/sequencer to get some info\n"
"Probably there is another program using it"
msgstr ""
"Kunde inte �ppna /dev/sequencer f�r att f� information.\n"
"Antagligen s� anv�nders den av ett annat program"

# kmidframe.cpp:639 kmidframe.cpp:640
#: kmidframe.cpp:681 kmidframe.cpp:682
#, c-format
msgid ""
"File %s already exists\n"
"Do you want to overwrite it ?"
msgstr ""
"Fil %s existerar redan.\n"
"Vill du skriva �ver den?"

# kmidclient.cpp:93
#: kmidclient.cpp:96
msgid "Tempo :"
msgstr "Tempo :"

# kmidclient.cpp:250
#: kmidclient.cpp:272
#, c-format
msgid "The file %s doesn't exist or can't be opened"
msgstr "Filen %s existerar inte eller s� kan den inte �ppnas"

# kmidclient.cpp:253
#: kmidclient.cpp:276
#, c-format
msgid "The file %s is not a midi file"
msgstr "Filen %s �r inte en midifil"

# kmidclient.cpp:255
#: kmidclient.cpp:278
msgid ""
"Ticks per cuarter note is negative, please, send this file to "
"antlarr@arrakis.es"
msgstr ""
"Takter per kvartsnot �r negativ. Var v�nlig och skicka denna\n"
"fil till antlarr@arrakis.es"

# kmidclient.cpp:257
#: kmidclient.cpp:280
msgid "Not enough memory !!"
msgstr "Inte tillr�ckligt minne !!"

# kmidclient.cpp:259
#: kmidclient.cpp:282
msgid "This file is corrupted or not well built"
msgstr "Denna fil �r felaktig eller d�ligt uppbyggd"

# kmidclient.cpp:260
#: kmidclient.cpp:283
msgid "Unknown error message"
msgstr "Ok�nt felmeddelande"

# kmidclient.cpp:439
#: kmidclient.cpp:467
msgid "You must load a file before playing it"
msgstr "Du m�ste ladda in en fil innan du kan spela den"

# kmidclient.cpp:445
#: kmidclient.cpp:473
msgid "A song is already being played"
msgstr "En s�ng h�ller redan p� att spelas upp"

# kmidclient.cpp:451
#: kmidclient.cpp:479
msgid ""
"Couldn't open /dev/sequencer\n"
"Probably there is another program using it"
msgstr ""
"Kunde inte �ppna /dev/sequencer\n"
"Antagligen anv�nds den av ett annat program"

# midicfgdlg.cpp:37
#: midicfgdlg.cpp:36
msgid "Configure Midi devices"
msgstr "Konfigurera midi-enheter"

# midicfgdlg.cpp:47
#: midicfgdlg.cpp:46
msgid "Select the midi device you want to use :"
msgstr "V�lj den midi-enhet du vill anv�nda :"

# midicfgdlg.cpp:67
#: midicfgdlg.cpp:66
msgid "Use the midi map :"
msgstr "Anv�nd midi map :"

# midicfgdlg.cpp:84 midicfgdlg.cpp:94 midicfgdlg.cpp:126
#: midicfgdlg.cpp:83 midicfgdlg.cpp:93 midicfgdlg.cpp:125
msgid "None"
msgstr "Ingen"

# collectdlg.cpp:39
#: collectdlg.cpp:39
msgid "Collections Manager"
msgstr "Samlingshanterare"

# collectdlg.cpp:47
#: collectdlg.cpp:47
msgid "Available collections :"
msgstr "Tillg�ngliga samlingar :"

# collectdlg.cpp:67
#: collectdlg.cpp:67
msgid "Songs in selected collection:"
msgstr "S�nger i vald samling:"

# collectdlg.cpp:81
#: collectdlg.cpp:81
msgid "New"
msgstr "Ny"

# collectdlg.cpp:151
#: collectdlg.cpp:154
msgid "Enter the name of the new collection"
msgstr "Skriv in namnet p� den nya samlingen"

# collectdlg.cpp:152
#: collectdlg.cpp:155
msgid "New Collection"
msgstr "Ny samling"

# collectdlg.cpp:159 collectdlg.cpp:185 collectdlg.cpp:226
#: collectdlg.cpp:162 collectdlg.cpp:188 collectdlg.cpp:229
#, c-format
msgid "The name '%s' is already used"
msgstr "Namnet '%s' anv�nds redan"

# collectdlg.cpp:177
#: collectdlg.cpp:180
msgid "Enter the name of the copy collection"
msgstr "Skriv in namnet p� samlingskopian"

# collectdlg.cpp:178
#: collectdlg.cpp:181
msgid "Copy Collection"
msgstr "Samlingskopia"

# collectdlg.cpp:219
#: collectdlg.cpp:222
msgid "Enter the new name for the selected collection"
msgstr "Skriv in det nya namnet p� vald samling"

# collectdlg.cpp:220
#: collectdlg.cpp:223
msgid "Change Collection Name"
msgstr "�ndra salingsnamnet"

# kmidframe.cpp:198
#: channelview.cpp:33
#, fuzzy
msgid "ChannelView"
msgstr "Visa kanalvy"

# channel.cpp:75
#: channel.cpp:87
msgid "Channel"
msgstr "Kanal"

# channelcfgdlg.cpp:12
#: channelcfgdlg.cpp:16
msgid "Configure Channel View"
msgstr "Konfigurera kanal vy"

# channelcfgdlg.cpp:22
#: channelcfgdlg.cpp:26
msgid "Choose look mode"
msgstr "V�lj utseendel�ge"

# channelcfgdlg.cpp:23
#: channelcfgdlg.cpp:27
msgid "3D look"
msgstr "3D utseende"

# channelcfgdlg.cpp:24
#: channelcfgdlg.cpp:28
msgid "3D - filled"
msgstr "3D - fylld"

# instrname.i18n:6
#: instrname.i18n:6
msgid "Acoustic Grand Piano"
msgstr "Acoustic Grand Piano"

# instrname.i18n:7
#: instrname.i18n:7
msgid "Bright Acoustic Piano"
msgstr "Bright Acoustic Piano"

# instrname.i18n:8
#: instrname.i18n:8
msgid "Electric Grand Piano"
msgstr "Electric Grand Piano"

# instrname.i18n:9
#: instrname.i18n:9
msgid "Honky-Tonk"
msgstr "Honky-Tonk"

# instrname.i18n:10
#: instrname.i18n:10
msgid "Rhodes Piano"
msgstr "Rhodes Piano"

# instrname.i18n:11
#: instrname.i18n:11
msgid "Chorused Piano"
msgstr "Chorused Piano"

# instrname.i18n:12
#: instrname.i18n:12
msgid "Harpsichord"
msgstr "Harpsichord"

# instrname.i18n:13
#: instrname.i18n:13
msgid "Clavinet"
msgstr "Clavinet"

# instrname.i18n:14
#: instrname.i18n:14
msgid "Celesta"
msgstr "Celesta"

# instrname.i18n:15
#: instrname.i18n:15
msgid "Glockenspiel"
msgstr "Glockenspiel"

# instrname.i18n:16
#: instrname.i18n:16
msgid "Music Box"
msgstr "Music Box"

# instrname.i18n:17
#: instrname.i18n:17
msgid "Vibraphone"
msgstr "Vibraphone"

# instrname.i18n:18
#: instrname.i18n:18
msgid "Marimba"
msgstr "Marimba"

# instrname.i18n:19
#: instrname.i18n:19
msgid "Xylophone"
msgstr "Xylophone"

# instrname.i18n:20
#: instrname.i18n:20
msgid "Tubular Bells"
msgstr "Tubular Bells"

# instrname.i18n:21
#: instrname.i18n:21
msgid "Dulcimer"
msgstr "Dulcimer"

# instrname.i18n:22
#: instrname.i18n:22
msgid "Hammond Organ"
msgstr "Hammond Organ"

# instrname.i18n:23
#: instrname.i18n:23
msgid "Percussive Organ"
msgstr "Percussive Organ"

# instrname.i18n:24
#: instrname.i18n:24
msgid "Rock Organ"
msgstr "Rock Organ"

# instrname.i18n:25
#: instrname.i18n:25
msgid "Church Organ"
msgstr "Church Organ"

# instrname.i18n:26
#: instrname.i18n:26
msgid "Reed Organ"
msgstr "Reed Organ"

# instrname.i18n:27
#: instrname.i18n:27
msgid "Accordion"
msgstr "Accordion"

# instrname.i18n:28
#: instrname.i18n:28
msgid "Harmonica"
msgstr "Harmonica"

# instrname.i18n:29
#: instrname.i18n:29
msgid "Tango Accordion"
msgstr "Tango Accordion"

# instrname.i18n:30
#: instrname.i18n:30
msgid "Acoustic Guitar (Nylon)"
msgstr "Acoustic Guitar (Nylon)"

# instrname.i18n:31
#: instrname.i18n:31
msgid "Acoustic Guitar (Steel)"
msgstr "Acoustic Guitar (Steel)"

# instrname.i18n:32
#: instrname.i18n:32
msgid "Electric Guitar (Jazz)"
msgstr "Electric Guitar (Jazz)"

# instrname.i18n:33
#: instrname.i18n:33
msgid "Electric Guitar (Clean)"
msgstr "Electric Guitar (Clean)"

# instrname.i18n:34
#: instrname.i18n:34
msgid "Electric Guitar (Muted)"
msgstr "Electric Guitar (Muted)"

# instrname.i18n:35
#: instrname.i18n:35
msgid "Overdriven Guitar"
msgstr "Overdriven Guitar"

# instrname.i18n:36
#: instrname.i18n:36
msgid "Distortion Guitar"
msgstr "Distortion Guitar"

# instrname.i18n:37
#: instrname.i18n:37
msgid "Guitar Harmonics"
msgstr "Guitar Harmonics"

# instrname.i18n:38
#: instrname.i18n:38
msgid "Acoustic Bass"
msgstr "Acoustic Bass"

# instrname.i18n:39
#: instrname.i18n:39
msgid "Electric Bass (Finger)"
msgstr "Electric Bass (Finger)"

# instrname.i18n:40
#: instrname.i18n:40
msgid "Electric Bass (Pick)"
msgstr "Electric Bass (Pick)"

# instrname.i18n:41
#: instrname.i18n:41
msgid "Fretless Bass"
msgstr "Fretless Bass"

# instrname.i18n:42
#: instrname.i18n:42
msgid "Slap Bass 1"
msgstr "Slap Bass 1"

# instrname.i18n:43
#: instrname.i18n:43
msgid "Slap Bass 2"
msgstr "Slap Bass 2"

# instrname.i18n:44
#: instrname.i18n:44
msgid "Synth Bass 1"
msgstr "Synth Bass 1"

# instrname.i18n:45
#: instrname.i18n:45
msgid "Synth Bass 2"
msgstr "Synth Bass 2"

# instrname.i18n:46
#: instrname.i18n:46
msgid "Violin"
msgstr "Violin"

# instrname.i18n:47
#: instrname.i18n:47
msgid "Viola"
msgstr "Viola"

# instrname.i18n:48
#: instrname.i18n:48
msgid "Cello"
msgstr "Cello"

# instrname.i18n:49
#: instrname.i18n:49
msgid "Contrabass"
msgstr "Contrabass"

# instrname.i18n:50
#: instrname.i18n:50
msgid "Tremolo Strings"
msgstr "Tremolo Strings"

# instrname.i18n:51
#: instrname.i18n:51
msgid "Pizzicato Strings"
msgstr "Pizzicato Strings"

# instrname.i18n:52
#: instrname.i18n:52
msgid "Orchestral Harp"
msgstr "Orchestral Harp"

# instrname.i18n:53
#: instrname.i18n:53
msgid "Timpani"
msgstr "Timpani"

# instrname.i18n:54
#: instrname.i18n:54
msgid "String Ensemble 1"
msgstr "String Ensemble 1"

# instrname.i18n:55
#: instrname.i18n:55
msgid "String Ensemble 2"
msgstr "String Ensemble 2"

# instrname.i18n:56
#: instrname.i18n:56
msgid "Synth Strings 1"
msgstr "Synth Strings 1"

# instrname.i18n:57
#: instrname.i18n:57
msgid "Synth Strings 2"
msgstr "Synth Strings 2"

# instrname.i18n:58
#: instrname.i18n:58
msgid "Choir Aahs"
msgstr "Choir Aahs"

# instrname.i18n:59
#: instrname.i18n:59
msgid "Voice Oohs"
msgstr "Voice Oohs"

# instrname.i18n:60
#: instrname.i18n:60
msgid "Synth Voice"
msgstr "Synth Voice"

# instrname.i18n:61
#: instrname.i18n:61
msgid "Orchestra Hit"
msgstr "Orchestra Hit"

# instrname.i18n:62
#: instrname.i18n:62
msgid "Trumpet"
msgstr "Trumpet"

# instrname.i18n:63
#: instrname.i18n:63
msgid "Trombone"
msgstr "Trombone"

# instrname.i18n:64
#: instrname.i18n:64
msgid "Tuba"
msgstr "Tuba"

# instrname.i18n:65
#: instrname.i18n:65
msgid "Muted Trumpet"
msgstr "Muted Trumpet"

# instrname.i18n:66
#: instrname.i18n:66
msgid "French Horn"
msgstr "French Horn"

# instrname.i18n:67
#: instrname.i18n:67
msgid "Brass Section"
msgstr "Brass Section"

# instrname.i18n:68
#: instrname.i18n:68
msgid "Synth Brass 1"
msgstr "Synth Brass 1"

# instrname.i18n:69
#: instrname.i18n:69
msgid "Synth Brass 2"
msgstr "Synth Brass 2"

# instrname.i18n:70
#: instrname.i18n:70
msgid "Soprano Sax"
msgstr "Soprano Sax"

# instrname.i18n:71
#: instrname.i18n:71
msgid "Alto Sax"
msgstr "Alto Sax"

# instrname.i18n:72
#: instrname.i18n:72
msgid "Tenor Sax"
msgstr "Tenor Sax"

# instrname.i18n:73
#: instrname.i18n:73
msgid "Baritone Sax"
msgstr "Baritone Sax"

# instrname.i18n:74
#: instrname.i18n:74
msgid "Oboe"
msgstr "Oboe"

# instrname.i18n:75
#: instrname.i18n:75
msgid "English Horn"
msgstr "English Horn"

# instrname.i18n:76
#: instrname.i18n:76
msgid "Bassoon"
msgstr "Bassoon"

# instrname.i18n:77
#: instrname.i18n:77
msgid "Clarinet"
msgstr "Clarinet"

# instrname.i18n:78
#: instrname.i18n:78
msgid "Piccolo"
msgstr "Piccolo"

# instrname.i18n:79
#: instrname.i18n:79
msgid "Flute"
msgstr "Flute"

# instrname.i18n:80
#: instrname.i18n:80
msgid "Recorder"
msgstr "Recorder"

# instrname.i18n:81
#: instrname.i18n:81
msgid "Pan Flute"
msgstr "Pan Flute"

# instrname.i18n:82
#: instrname.i18n:82
msgid "Blown Bottle"
msgstr "Blown Bottle"

# instrname.i18n:83
#: instrname.i18n:83
msgid "Shakuhachi"
msgstr "Shakuhachi"

# instrname.i18n:84
#: instrname.i18n:84
msgid "Whistle"
msgstr "Whistle"

# instrname.i18n:85
#: instrname.i18n:85
msgid "Ocarina"
msgstr "Ocarina"

# instrname.i18n:86
#: instrname.i18n:86
msgid "Lead 1 - Square Wave"
msgstr "Lead 1 - Square Wave"

# instrname.i18n:87
#: instrname.i18n:87
msgid "Lead 2 - Saw Tooth"
msgstr "Lead 2 - Saw Tooth"

# instrname.i18n:88
#: instrname.i18n:88
msgid "Lead 3 - Calliope"
msgstr "Lead 3 - Calliope"

# instrname.i18n:89
#: instrname.i18n:89
msgid "Lead 4 - Chiflead"
msgstr "Lead 4 - Chiflead"

# instrname.i18n:90
#: instrname.i18n:90
msgid "Lead 5 - Charang"
msgstr "Lead 5 - Charang"

# instrname.i18n:91
#: instrname.i18n:91
msgid "Lead 6 - Voice"
msgstr "Lead 6 - Voice"

# instrname.i18n:92
#: instrname.i18n:92
msgid "Lead 7 - Fifths"
msgstr "Lead 7 - Fifths"

# instrname.i18n:93
#: instrname.i18n:93
msgid "Lead 8 - Bass+Lead"
msgstr "Lead 8 - Bass+Lead"

# instrname.i18n:94
#: instrname.i18n:94
msgid "Pad 1 - New Age"
msgstr "Pad 1 - New Age"

# instrname.i18n:95
#: instrname.i18n:95
msgid "Pad 2 - Warm"
msgstr "Pad 2 - Warm"

# instrname.i18n:96
#: instrname.i18n:96
msgid "Pad 3 - Polysynth"
msgstr "Pad 3 - Polysynth"

# instrname.i18n:97
#: instrname.i18n:97
msgid "Pad 4 - Choir"
msgstr "Pad 4 - Choir"

# instrname.i18n:98
#: instrname.i18n:98
msgid "Pad 5 - Bow"
msgstr "Pad 5 - Bow"

# instrname.i18n:99
#: instrname.i18n:99
msgid "Pad 6 - Metallic"
msgstr "Pad 6 - Metallic"

# instrname.i18n:100
#: instrname.i18n:100
msgid "Pad 7 - Halo"
msgstr "Pad 7 - Halo"

# instrname.i18n:101
#: instrname.i18n:101
msgid "Pad 8 - Sweep"
msgstr "Pad 8 - Sweep"

# instrname.i18n:102
#: instrname.i18n:102
msgid "FX 1 - Rain"
msgstr "FX 1 - Rain"

# instrname.i18n:103
#: instrname.i18n:103
msgid "FX 2 - Soundtrack"
msgstr "FX 2 - Soundtrack"

# instrname.i18n:104
#: instrname.i18n:104
msgid "FX 3 - Crystal"
msgstr "FX 3 - Crystal"

# instrname.i18n:105
#: instrname.i18n:105
msgid "FX 4 - Atmosphere"
msgstr "FX 4 - Atmosphere"

# instrname.i18n:106
#: instrname.i18n:106
msgid "FX 5 - Brightness"
msgstr "FX 5 - Brightness"

# instrname.i18n:107
#: instrname.i18n:107
msgid "FX 6 - Goblins"
msgstr "FX 6 - Goblins"

# instrname.i18n:108
#: instrname.i18n:108
msgid "FX 7 - Echoes"
msgstr "FX 7 - Echoes"

# instrname.i18n:109
#: instrname.i18n:109
msgid "FX 8 - Sci-fi"
msgstr "FX 8 - Sci-fi"

# instrname.i18n:110
#: instrname.i18n:110
msgid "Sitar"
msgstr "Sitar"

# instrname.i18n:111
#: instrname.i18n:111
msgid "Banjo"
msgstr "Banjo"

# instrname.i18n:112
#: instrname.i18n:112
msgid "Shamisen"
msgstr "Shamisen"

# instrname.i18n:113
#: instrname.i18n:113
msgid "Koto"
msgstr "Koto"

# instrname.i18n:114
#: instrname.i18n:114
msgid "Kalimba"
msgstr "Kalimba"

# instrname.i18n:115
#: instrname.i18n:115
msgid "Bagpipe"
msgstr "Bagpipe"

# instrname.i18n:116
#: instrname.i18n:116
msgid "Fiddle"
msgstr "Fiddle"

# instrname.i18n:117
#: instrname.i18n:117
msgid "Shannai"
msgstr "Shannai"

# instrname.i18n:118
#: instrname.i18n:118
msgid "Tinkle Bell"
msgstr "Tinkle Bell"

# instrname.i18n:119
#: instrname.i18n:119
msgid "Agogo"
msgstr "Agogo"

# instrname.i18n:120
#: instrname.i18n:120
msgid "Steel Drum"
msgstr "Steel Drum"

# instrname.i18n:121
#: instrname.i18n:121
msgid "Wook Block"
msgstr "Wook Block"

# instrname.i18n:122
#: instrname.i18n:122
msgid "Taiko Drum"
msgstr "Taiko Drum"

# instrname.i18n:123
#: instrname.i18n:123
msgid "Melodic Tom"
msgstr "Melodic Tom"

# instrname.i18n:124
#: instrname.i18n:124
msgid "Synth Drum"
msgstr "Synth Drum"

# instrname.i18n:125
#: instrname.i18n:125
msgid "Reverse Cymbal"
msgstr "Reverse Cymbal"

# instrname.i18n:126
#: instrname.i18n:126
msgid "Guitar Fret Noise"
msgstr "Guitar Fret Noise"

# instrname.i18n:127
#: instrname.i18n:127
msgid "Breath Noise"
msgstr "Breath Noise"

# instrname.i18n:128
#: instrname.i18n:128
msgid "Seashore"
msgstr "Seashore"

# instrname.i18n:129
#: instrname.i18n:129
msgid "Bird Tweet"
msgstr "Bird Tweet"

# instrname.i18n:130
#: instrname.i18n:130
msgid "Telephone"
msgstr "Telephone"

# instrname.i18n:131
#: instrname.i18n:131
msgid "Helicopter"
msgstr "Helicopter"

# instrname.i18n:132
#: instrname.i18n:132
msgid "Applause"
msgstr "Applause"

# instrname.i18n:133
#: instrname.i18n:133
msgid "Gunshot"
msgstr "Gunshot"

# kmidframe.cpp:62
#~ msgid "&Open ..."
#~ msgstr "&�ppna ..."

# kmidframe.cpp:68
#~ msgid "&Quit"
#~ msgstr "&Avsluta"

# kmidframe.cpp:160
#~ msgid "&File"
#~ msgstr "&Fil"

# kmidframe.cpp:163
#~ msgid "&Options"
#~ msgstr "&Inst�llningar"

# kmidframe.cpp:165
#~ msgid "&Help"
#~ msgstr "&Hj�lp"

# kmidframe.cpp:179
#~ msgid "Stop"
#~ msgstr "Stop"

# kmidclient.cpp:262 kmidclient.cpp:451 kmidframe.cpp:520
#~ msgid "Error"
#~ msgstr "Fel"

# collectdlg.cpp:161 collectdlg.cpp:187 collectdlg.cpp:228 kmidclient.cpp:438
# kmidclient.cpp:444 kmidframe.cpp:641
#~ msgid "Warning"
#~ msgstr "Varning"

# channelcfgdlg.cpp:15 collectdlg.cpp:40 kaskdlg.cpp:37 midicfgdlg.cpp:40
#~ msgid "OK"
#~ msgstr "OK"

# channelcfgdlg.cpp:18 collectdlg.cpp:43 kaskdlg.cpp:40 midicfgdlg.cpp:43
#~ msgid "Cancel"
#~ msgstr "Avbryt"

# midicfgdlg.cpp:89
#~ msgid "Browse..."
#~ msgstr "Bl�ddra..."

# collectdlg.cpp:85
#~ msgid "Copy"
#~ msgstr "Kopiera"

# collectdlg.cpp:89
#~ msgid "Delete"
#~ msgstr "Radera"

# collectdlg.cpp:94
#~ msgid "Add"
#~ msgstr "L�gg till"

# collectdlg.cpp:98
#~ msgid "Remove"
#~ msgstr "Ta bort"
